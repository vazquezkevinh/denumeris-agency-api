<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Postal Codes API
    |--------------------------------------------------------------------------
    |
    | Mensajes en español para api de codigos postales
    |
    */

    'zip_code_malformed' => 'Código Postal incorrecto',
    'zip_code_not_found' => 'Código Postal no localizado',
    'zip_code_error' => 'Ocurrió un error mientras se procesaba la solicitud',
    'titles' => [
        'key' => 'clave',
        'name' => 'nombre',
        'federal_entity' => 'Estado',
        'municipality' => 'Municipio',
        'settlement' => 'Asentamiento',
        'order' => 'Ordenamiento',
        'zip_code' => 'Codigo postal'
    ]
];
