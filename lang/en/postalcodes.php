<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Postal Codes API
    |--------------------------------------------------------------------------
    |
    | Mensajes en español para api de codigos postales
    |
    */

    'zip_code_malformed' => 'Postal Code malformed',
    'zip_code_not_found' => 'Postal Code not found',
    'zip_code_error' => 'An error occurred while processing your request',
];
