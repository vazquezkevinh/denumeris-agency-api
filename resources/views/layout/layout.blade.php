<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->
    <head>
        <!--begin::Base Path (base relative path for assets of this page) -->
        <base href="../">
        <!--end::Base Path -->
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="Updates and statistics">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Denumeris Agency API</title>

        <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('favicon.ico') }}" sizes="48X16">
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css')}} " rel="stylesheet">

        <!-- Additional CSS Files -->
        <link rel="stylesheet" href="{{ asset('css/fontawesome.css') }}">
        <link rel="stylesheet" href="{{ asset('css/templatemo-eduwell-style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/owl.css') }}">
        <link rel="stylesheet" href="{{ asset('css/lightbox.css') }}">

        <link href="{{ asset('css/layout.css') }}" rel="stylesheet">
        @yield('styles')

</head>
<!-- end::Head -->
<!-- begin::Body -->
<body>
    @include('layout.header')

    @yield('content')

    <script type="text/javascript">
            // var global URL
            const url = '{!! URL::asset("") !!}';
    </script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-CEOnF9jw1gnh746IfcvJY0fX77h2ssM"></script>
    <script src="{{ URL::asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/sweetalert.js') }}"></script>
    <script src="{{ URL::asset('js/blockui.js') }}"></script>
    <script src="{{ URL::asset('js/app.js') }}"></script>
    @yield('scripts')
        
</body>

</html>
