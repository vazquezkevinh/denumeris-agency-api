@extends('layout.layout')

@section('content')
 
    <section class="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <form id="contact" action="" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section-heading">
                                <h6>Precios de gasolina</h6>
                                <h4>Kevin <em>Vázquez</em></h4>
                                <p>Por favor seleccina entre las opciones que se encuentran debajo para mostrar los precios de gasolina.</p>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <select class="selectLocations" name="fedetalEntity" id="fedetalEntity">
                                        <option value="">Seleccione una opción</option>
                                        @foreach($federalEntities as $entity)
                                            <option value="{{ $entity->c_estado }}">{{ $entity->d_estado }}</option>
                                        @endforeach

                                    </select>
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <select class="selectLocations" name="municipalities" id="municipalities">
                                        <option value="">Seleccione una opción</option>
                                    </select>
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <select class="selectLocations" name="settlements" id="settlements">
                                        <option value="">Seleccione una opción</option>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-8">

                    <div id="map" style="height: 500px; width: auto;"></div>
                    
                </div>

                <div class="col-lg-12 d-none" id="divGasStations">

                    <table class="table responsive" id="tblGasStations">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Calle</th>
                                <th scope="col">regular</th>
                                <th scope="col">premium</th>
                                <th scope="col">dieasel</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyGasStations">

                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12">
                    <ul class="social-icons">
                        <li><a href="https://www.linkedin.com/in/kevin-h-vazquez" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="mailto:vazquezkevinh@gmail.com"><i class="fa fa-envelope-o"></i></a></li>
                        <li><a href="tel:5523325116"><i class="fa fa-phone"></i></a></li>
                        <li><a href="https://gitlab.com/vazquezkevinh"><i class="fa fa-github" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="col-lg-12">
                    <p class="copyright">Copyright © 2022 EduWell Co., Ltd. All Rights Reserved. 
                    
                    <br>Design: <a rel="sponsored" href="https://templatemo.com" target="_blank">TemplateMo</a></p>
                </div>
            </div>
        </div>
    </section>

    @section('scripts')
        <script src="{{ URL::asset('js/locations.js') }}" type="text/javascript"></script>
        {{-- <script src="{{ URL::asset('js/dashboard.js?v=1.0.3')}}" type="module"></script>
        <script type="text/javascript"> 
            var $mostarEncuesta = "{{ $mostrar_encuesta }}"; 
            if($mostarEncuesta){
                $('#btnShowModal1').trigger('click');
            }
        </script> --}}
    @endsection


@endsection