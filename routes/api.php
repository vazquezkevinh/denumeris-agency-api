<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostalCodesController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::prefix('locations')->middleware(['json.responses'])->group(function () {
Route::prefix('locations')->group(function () {
    Route::post('getInfoLocation', [PostalCodesController::class, 'getInfoLocation']);
    Route::post('getInfoByFederalEntities/{id}', [PostalCodesController::class, 'getInfoByFederalEntities']);
    Route::post('getInfoByMunicipality/{idFederalEntity}/{idMunicipality}', [PostalCodesController::class, 'getInfoByMunicipality']);
    Route::post('getGasStations/{cp}', [PostalCodesController::class, 'getGasStations']);
});
