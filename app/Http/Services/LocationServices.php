<?php

namespace App\Http\Services;

//Models
use App\Models\PostalCode;

//Request
use App\Http\Requests\LocationFinder;

//Guzzle
use GuzzleHttp\Client;

class LocationServices
{
    //default response code
    private $status_code = 200;
    private $postalCodeModel;

    public function __construct(){
        $this->postalCodeModel = new PostalCode();
    }

    public function getInfoLocation(Object $request){
        $zip_code           = (isset($request->zip_code)) ? $request->zip_code : null;
        $federal_entity_id  = $request->federal_entity;
        $municipality_id    = $request->municipality;
        $order              = (isset($request->order)) ? $request->order : null;
        //Try to find some information
        try{
            //Look for info from database
            $postalCodeModel = new PostalCode();

            //just filter by CP
            if($zip_code != null){
                $postalCodeModel = $postalCodeModel->where('d_codigo', $zip_code );
            }else{
                $postalCodeModel = 
                //filter by federal_entity
                $postalCodeModel->where('c_estado', $federal_entity_id )
                
                //filter by municipality
                ->where('c_mnpio', $municipality_id );
            }

            $locations = $postalCodeModel->get();

            if(!$locations->isEmpty()){

                $settlements = array();

                foreach($locations as $location){
                    array_push($settlements, 
                        array(
                            "clave" => $location->id_asenta_cpcons,
                            "nombre" => $this->removeAccents($location->d_asenta),
                            "tipo_zona" => strtoupper($location->d_zona),
                            "tipo_asentamiento" => array(
                                "nombre" => $location->d_tipo_asenta
                            )
                        )
                    );
                }

                $response = array(
                    "codigo_postal" => $zip_code,
                    "ciudad" => $this->removeAccents($locations[0]->d_ciudad),
                    "estado" => array (
                        "clave" => $locations[0]->c_estado,
                        "nombre" => $this->removeAccents($locations[0]->d_estado),
                    ),
                    "colonias" => $settlements,
                    "municipio"=> array(
                        "clave"=> $locations[0]->c_mnpio,
                        "nombre"=> $this->removeAccents($locations[0]->d_mnpio)
                    )
                );

                //Successfull response
                $this->status_code = 200;

            }else{
                //Data not found
                $this->status_code = 404;

                $response = array(
                    'status' => false,
                    'error_message' => __('postalcodes.zip_code_not_found')
                );
            }

        }catch(\Exception $e){
            //Server error
            $this->status_code = 400;

            $response = array(
                'status' => false,
                'error_message' => __('postalcodes.zip_code_error')
            );
        }

        //Send response
        return response()->json($response,$this->status_code, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],JSON_UNESCAPED_UNICODE);

    }

    public function getInfoByFederalEntities($id){
        try{
            //Look for info from database
            $postalCodeModel = new PostalCode();
            $data = $postalCodeModel->getMunicipalityByFederalEntity($id);
            if(!$data->isEmpty()){
                $response = array(
                    'status' => true,
                    'data' => $data
                );
                //Successfull response
                $this->status_code = 200;
            }else{
                $response = array(
                    'status' => false,
                    'error_message' => __('postalcodes.zip_code_error')
                );
                //Not found response
                $this->status_code = 404;
            }

        }catch(\Exception $e){
            //Server error
            $this->status_code = 400;

            $response = array(
                'status' => false,
                'error_message' => __('postalcodes.zip_code_error')
            );
        }

        //Send response
        return response()->json($response,$this->status_code, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],JSON_UNESCAPED_UNICODE);
    }

    public function getInfoByMunicipality($idFederalEntity, $idMunicipality){
        try{
            //Look for info from database
            $postalCodeModel = new PostalCode();
            $data = $postalCodeModel->getInfoByFederalAndMunicipality($idFederalEntity, $idMunicipality);
            if(!$data->isEmpty()){
                $response = array(
                    'status' => true,
                    'data' => $data
                );
                //Successfull response
                $this->status_code = 200;
            }else{
                $response = array(
                    'status' => false,
                    'error_message' => __('postalcodes.zip_code_error')
                );
                //Not found response
                $this->status_code = 404;
            }

        }catch(\Exception $e){
            //Server error
            $this->status_code = 400;

            $response = array(
                'status' => false,
                'error_message' => __('postalcodes.zip_code_error')
            );
        }

        //Send response
        return response()->json($response,$this->status_code, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],JSON_UNESCAPED_UNICODE);
    }

    public function getGasStations($cp){

        try{
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => env('API_GASSTATIONS_URL','https://api.datos.gob.mx/v1/'),
                // You can set any number of default request options.
                'timeout'  => 2.0,
            ]);

            $response = $client->request('GET', 'precio.gasolina.publico', [
                'query' => ['codigopostal' => $cp]
            ]);
            
            if($response->getStatusCode() === 200){
                $body = json_decode($response->getBody()->getContents(), true );

                if(isset($body['results']) && sizeof($body['results']) > 0){
                    //Successfull response
                    $response = array(
                        'status' => true,
                        'data' => $body['results']
                    );
                }else{
                    // no results
                    $response = array(
                        'status' => false,
                        'error_message' => __('postalcodes.zip_code_error')
                    );
                    //Not found response
                    //$this->status_code = 404;
                }
            }else{
                $response = array(
                    'status' => false,
                    'error_message' => __('postalcodes.zip_code_error')
                );
                //Not found response
                //$this->status_code = 404;
            }

        }catch(\Exception $e){
            //Server error
            //$this->status_code = 400;

            $response = array(
                'status' => false,
                'error_message' => __('postalcodes.zip_code_error')
            );
        }

        //Send response
        return response()->json($response,$this->status_code, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],JSON_UNESCAPED_UNICODE);
    }

    public function removeAccents($str){
        $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
        $str = strtr( $str, $unwanted_array );

        return strtoupper($str);
    }

}
