<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Models
use App\Models\PostalCode;

//Request
use App\Http\Requests\LocationFinder;

//Services
use App\Http\Services\LocationServices;

class PostalCodesController extends Controller
{
    
    private $objLocation;

    public function __construct()
    {
        $this->objLocation = new LocationServices();
    }

    /* Return index data */
    public function index(){
        $federalEntities = PostalCode::getFederalEntities();
        return view('locations.locations')->with('federalEntities', $federalEntities);
    }

    public function getInfoByFederalEntities(Request $request, $id){
        return $this->objLocation->getInfoByFederalEntities($id);
    }

    public function getInfoByMunicipality(Request $request, $idFederalEntity, $idMunicipality){
        return $this->objLocation->getInfoByMunicipality($idFederalEntity, $idMunicipality);
    }

    public function getGasStations(Request $request, $cp){
        return $this->objLocation->getGasStations($cp);
    }

    /* Return information from a certain CP */
    public function getInfoLocation(LocationFinder $request){
        return $this->objLocation->getInfoLocation((Object)$request->all());
    }

}
