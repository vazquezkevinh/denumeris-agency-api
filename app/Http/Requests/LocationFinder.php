<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocationFinder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'federal_entity' => 'required|numeric',
            'municipality'   => 'required|numeric',
            'order'          => 'sometimes|min:3|max:40',
            'zip_code'       => 'sometimes|digits:5'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes(){
        return [
            'federal_entity' => __('postalcodes.titles.federal_entity'),
            'municipality'   => __('postalcodes.titles.municipality'),
            'order'          => __('postalcodes.titles.order'),
            'zip_code'       => __('postalcodes.titles.zip_code'),
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages(){
        return [
            'numeric' => 'El campo :attribute debe ser numerico',
            'required' => 'El campo :attribute es requerido.',
            'digits' => 'El campo :attribute debe contener :digits caracteres.',
            'min' => 'El campo :attribute debe contener minimo :min caracteres.',
            'max' => 'El campo :attribute debe contener maximo :max caracteres.',
        ];
    }
}
