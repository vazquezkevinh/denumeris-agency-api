<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

//Cache
use Cache;
use Carbon\Carbon;

class PostalCode extends Model
{
    use HasFactory;
    protected $table = 'postal_codes';

    public static function getFederalEntities(){
        return self::federalEntitiesFromCache();
    }

    //get data from Entities Cache if not exist, create cache
    public static function federalEntitiesFromCache(){
        if(! Cache::has('federal_entities') ) {
            $tramites = PostalCode::select('c_estado', 'd_estado')->groupBy('c_estado')->groupBy('d_estado')->orderBy('d_estado')->get();
            Cache::put('federal_entities', $tramites, Carbon::now()->addDays(30));
        }

        return Cache::get('federal_entities');
    }

    //Filter municipalities bye federal entity id
    public static function getMunicipalityByFederalEntity($id){
        return PostalCode::select('c_estado', 'c_mnpio', 'd_mnpio')->where('c_estado', $id)->distinct('c_estado', 'c_mnpio', 'd_mnpio')->orderBy('d_mnpio')->get();
        //->pluck('c_estado', 'c_mnpio', 'd_mnpio')
        //return self::municipalitiesFromCache()->where('c_estado', $id);
    }

    //Filter municipalities bye federal entity id
    public static function getInfoByFederalAndMunicipality($idFederalEntity, $idMunicipality){
        return PostalCode::select('c_estado', 'c_mnpio', 'd_mnpio', 'd_asenta', 'd_codigo', 'id_asenta_cpcons')->where('c_estado', $idFederalEntity)->where('c_mnpio', $idMunicipality)->distinct('c_estado', 'c_mnpio', 'd_mnpio', 'd_asenta', 'd_codigo', 'id_asenta_cpcons')->orderBy('d_mnpio')->get();
    }

    //get data from Municipalities Cache if not exist, create cache
    public static function municipalitiesFromCache(){

        if(! Cache::has('minicipalities') ) {
            $tramites = PostalCode::select('c_estado', 'c_mnpio', 'd_mnpio')->distinct('c_estado', 'c_mnpio', 'd_mnpio')->orderBy('d_mnpio')->get();
            Cache::put('minicipalities', $tramites, Carbon::now()->addDays(30));
        }

        return Cache::get('minicipalities');

    }
}
