'use strict'

var $municipalityId;
var $federalEntityId;
var $locationsMap;
var $centerMap;

const getMunicipalities = (federalEntity) => {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url : url + "api/locations/getInfoByFederalEntities/"+federalEntity,
        type: 'POST',
        dataType: 'json',
        success: function(respuesta) {

            if(respuesta.status === true){
                //Successfull response
                $federalEntityId = federalEntity;
                
                var municipalities = respuesta.data;

                const municipalitiesSelect = document.querySelector('#municipalities');
                municipalitiesSelect.textContent = '';

                //Agregamos la opción por default
                var newOption = new Option('Seleccione una opción', '', false, false);
                municipalitiesSelect.append(newOption);

                municipalities.forEach(function(municipality, index) {

                    const opt = document.createElement('OPTION');
                    opt.setAttribute("value", municipality.c_mnpio)
                    var t = document.createTextNode(municipality.d_mnpio);
                    opt.appendChild(t);
                    municipalitiesSelect.appendChild(opt);
                    
                });

            }else{
                Swal.fire('¡Alerta!', 'Ocurrió un error.', 'warning');
            }
        },
        error: function(xhr) { //xhr
            if(xhr.responseJSON){
                if(xhr.responseJSON.errors){
                    imprimirMensajesDeError(xhr.responseJSON.errors);
                }
            }else{
                Swal.fire('¡Alerta!', 'Error de conectividad de red USR-02.', 'warning');
            }
        },

        beforeSend: () => {
            blockUI();
        },
        complete: () => {
            $.unblockUI();
        }
    });
}

const getSettlement = (municipality) => {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url : url + "api/locations/getInfoByMunicipality/"+$federalEntityId+"/"+municipality,
        type: 'POST',
        dataType: 'json',
        success: function(respuesta) {
            if(respuesta.status === true){

                $municipalityId = municipality;
                
                var settlements = respuesta.data;

                const settlementsSelect = document.querySelector('#settlements');
                settlementsSelect.textContent = '';

                //Agregamos la opción por default
                var newOption = new Option('Seleccione una opción', '', false, false);
                settlementsSelect.append(newOption);

                settlements.forEach(function(settlement, index) {

                    const opt = document.createElement('OPTION');
                    opt.setAttribute("value", settlement.id_asenta_cpcons)
                    opt.setAttribute("data-cp", settlement.d_codigo)
                    var t = document.createTextNode(settlement.d_asenta);
                    opt.appendChild(t);
                    settlementsSelect.appendChild(opt);
                    
                });

            }else{
                Swal.fire('¡Alerta!', 'No se localizaron asentamientos', 'warning');
            }
        },
        error: function(xhr) { //xhr
            if(xhr.responseJSON){
                if(xhr.responseJSON.errors){
                    imprimirMensajesDeError(xhr.responseJSON.errors);
                }
            }else{
                Swal.fire('¡Alerta!', 'Error de conectividad de red USR-02.', 'warning');
            }
        },

        beforeSend: () => {
            blockUI();
        },
        complete: () => {
            $.unblockUI();
        }
    });
}

const getGasStations = (settelmentCp) => {
    $locationsMap = [];

    //Default center location
    $centerMap = [19.432184, -99.133425];

    //Selector tbody
    const tbodyGasStations = document.querySelector('#tbodyGasStations');
    const divGasStations = document.querySelector('#divGasStations');

    tbodyGasStations.textContent = '';
    divGasStations.classList.add('d-none');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url : url + "api/locations/getGasStations/"+settelmentCp,
        type: 'POST',
        dataType: 'json',
        success: function(respuesta) {
            if(respuesta.status === true){

                const gasStations = respuesta.data;
                
                $locationsMap = [];

                let element = '';
                
                gasStations.forEach(function(gas, index) {

                    if(index == 0){
                        $centerMap = [
                            gas.latitude,
                            gas.longitude,
                        ];
                    }

                    $locationsMap.push([
                        gas.razonsocial,
                        gas.latitude,
                        gas.longitude,
                        index + 1
                    ]);

                    //table
                    element += `
                        <tr>
                            <th>${gas.calle}</th>
                            <th>${gas.regular}</th>
                            <th>${gas.premium}</th>
                            <th>${gas.dieasel}</th>
                        </tr>
                    `;

                    tbodyGasStations.innerHTML = element;
                });

                //display table
                divGasStations.classList.remove('d-none');

            }else{
                Swal.fire('¡Alerta!', 'No se localizaron gasolineras', 'warning');
            }
        },
        error: function(xhr) { //xhr
            if(xhr.responseJSON){
                if(xhr.responseJSON.errors){
                    imprimirMensajesDeError(xhr.responseJSON.errors);
                }
            }else{
                Swal.fire('¡Alerta!', 'Error de conectividad de red USR-02.', 'warning');
            }
        },

        beforeSend: () => {
            blockUI();
        },
        complete: () => {
            $.unblockUI();
            makeMap();
        }
    });

}

const makeMap = () => {

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: new google.maps.LatLng($centerMap[0], $centerMap[1]),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < $locationsMap.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng($locationsMap[i][1], $locationsMap[i][2]),
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent($locationsMap[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
}

document.addEventListener('DOMContentLoaded', () => {

    const federalEntitySelect = document.querySelector('#fedetalEntity');
    const municipalitiesSelect = document.querySelector('#municipalities');
    const settlementsSelect = document.querySelector('#settlements');

    federalEntitySelect.addEventListener('change', (event) => {
        const federalEntity = event.target.value;
        if(federalEntity !== ''){

            //Clean child inputs
            municipalitiesSelect.textContent = '';
            settlementsSelect.textContent = '';

            //Default option for both inputs
            var newOption = new Option('Seleccione una opción', '', false, false);
            var newOption2 = new Option('Seleccione una opción', '', false, false);
            settlementsSelect.append(newOption);
            municipalitiesSelect.append(newOption2);

            getMunicipalities(federalEntity);
        }else{
	        Swal.fire('¡Alerta!', 'Por favor selecciona un estado valido', 'error');
            return;
        }
    });

    municipalitiesSelect.addEventListener('change', (event) => {
        const municipality = event.target.value;
        if(municipality !== ''){
            getSettlement(municipality);
        }else{
	        Swal.fire('¡Alerta!', 'Por favor selecciona una opcion valida', 'error');
            return;
        }
    });

    settlementsSelect.addEventListener('change', (event) => {
        let settelmentCp = event.target.selectedOptions[0].getAttribute('data-cp');
        if(settelmentCp !== '' && settelmentCp != 'undefined' && settelmentCp !== null){
            settelmentCp = (settelmentCp.length == 4) ? settelmentCp.padStart(5, '0') : settelmentCp;
            getGasStations(settelmentCp);
        }else{
	        Swal.fire('¡Alerta!', 'No es un valor valido', 'error');
            return;
        }
    });
})