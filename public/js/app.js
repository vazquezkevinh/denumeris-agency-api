//Center the element
$.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
    this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
    return this;
}

//blockUI
function blockUI() {
    $.blockUI({
        css: {
        backgroundColor: 'transparent',
        border: 'none'
        },
        message: '<div class="spinner"></div>',
        baseZ: 1500,
        overlayCSS: {
        backgroundColor: '#FFFFFF',
        opacity: 0.7,
        cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}//end Blockui   

const imprimirMensajesDeError = (data) => {
	//Quitamos la clase de los input invalidos
	$(".is-invalid").removeClass("is-invalid");

	var errores = '';
	var mensaje;

	$.each(data, function (key, value) {
		$('#' + key + '').addClass('is-invalid');
		errores += '<li>' + value + '</li>';
	});

	mensaje = '<ul class="ulSwal">' + errores + '</ul>'
	Swal.fire('¡Alerta!', mensaje, 'warning');
}
