<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postal_codes', function (Blueprint $table) {
            $table->bigIncrements('id')->comments('Identificador de registro.');
            $table->integer('d_codigo')->comments('Código postal asentamiento');
            $table->string('d_asenta', 255)->comments('Nombre asentamiento');
            $table->string('d_tipo_asenta', 255)->comments('Tipo de asentamiento (Cátalogo SEPOMEX)');
            $table->string('d_mnpio', 255)->comments('Nombre Municipio (INEGI, Marzo 2013)');
            $table->string('d_estado', 255)->comments('Nombre Entidad (INEGI, Marzo 2013');
            $table->string('d_ciudad', 255)->comments('Nombre Ciudad (Catálogo SEPOMEX)')->nullable();
            $table->integer('d_cp')->comments('Código Postal de la Administración postal que reparte al asentamiento');
            $table->integer('c_estado')->comments('Clave Entidad (INEGI, Marzo 2013)');
            $table->integer('c_oficina')->comments('Código Postal de la Administración Postal que reparte al asentamiento');
            $table->string('c_cp', 255)->comments('Campo vacio')->nullable();
            $table->integer('c_tipo_asenta')->comments('Clave Tipo de Asentamiento (Catálogo SEPOMEX)');
            $table->integer('c_mnpio')->comments('Clave Municipio (INEGI, Marzo 2013)');
            $table->integer('id_asenta_cpcons')->comments('Identificafor único del asentamiento (nivel mundial)');
            $table->string('d_zona', 255)->comments('Zona en la que se ubica el asentamiento (Urbano/Rural)');
            $table->string('c_cve_ciudad', 255)->comments('Clave Ciudad (Catálogo SEPOMEX)')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postal_codes');
    }
};
