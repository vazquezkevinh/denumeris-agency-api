<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\PostalCode;
use Storage;

class PostalCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //default method
        $this->method1();
        
        // just if method 1 fails
        //$this->method2();
    }

    /*
        populate database with data from a txt file - reads each line
    */
    public function method1(){
        //just for insert
        $file = Storage::disk('public')->path('attachments/postal_codes.txt');
        //$file = dirname(__DIR__,2)."/storage/app/public/attachments/postal_codes_li.txt";

        foreach ($this->file_lines($file) as $key=>$line){

            $buffer = str_replace(array("\r", "\n"), '', $line);
            $array[$key] = explode('|', $buffer);
            //Just for deal with white spaces
            if(isset($array[$key][0])){
                PostalCode::create([
                    'd_codigo'         => $array[$key][0],
                    'd_asenta'         => $array[$key][1],
                    'd_tipo_asenta'    => $array[$key][2],
                    'd_mnpio'          => $array[$key][3],
                    'd_estado'         => $array[$key][4],
                    'd_ciudad'         => $array[$key][5],
                    'd_cp'             => $array[$key][6],
                    'c_estado'         => $array[$key][7],
                    'c_oficina'        => $array[$key][8],
                    'c_cp'             => $array[$key][9],
                    'c_tipo_asenta'    => $array[$key][10],
                    'c_mnpio'          => $array[$key][11],
                    'id_asenta_cpcons' => $array[$key][12],
                    'd_zona'           => $array[$key][13],
                    'c_cve_ciudad'     => $array[$key][14],
                ]);
            }

        }
    }

    /*
        populate database with dump from a previous database
    */
    public function method2(){
        //just for insert
        ini_set('memory_limit',-1);
        \DB::unprepared(\File::get(base_path('storage/app/public/attachments/postal_codes.sql')));
    }

    //iterate data from large files
    public function file_lines($filename) {
        $file = fopen($filename, 'r');
        while (($line = fgets($file)) !== false) {
            yield $line;
        }
        fclose($file);
    }
    
}
