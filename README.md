## Instalación
- Clonar el proyecto
- Copiar el archivo **.env.example** y nombrarlo **.env**
    - Configurar la conexión a base de datos **MySql**
    - Elegir tu idioma preferido **APP_LANG**
        - Idiomas disponibles **['es', 'en']**

- Ejecutar el comando **composer install**
- Ejecutar el comando **php artisan key:generate**
- Ejecutar el comando **php artisan migrate**
- Ejecutar el comando **php artisan db:seed**
    - Si el comando anterior llega a fallar deberás abrir el archivo **database/seeders/PostalCodesSeeder.php** y realizar los siguientes cambios
        - Comentar la línea 21  **$this->method1();**
        - Descomentar la línea 24  **$this->method2();**
        - Ejecutar nuevamente el comando **php artisan migrate:refresh --seed**
- Ejecutar el comando **php artisan serve**

## ¡Excelente! ahora puedes probar el API